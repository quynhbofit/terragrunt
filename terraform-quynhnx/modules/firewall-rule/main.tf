terraform {
  experiments = [module_variable_optional_attrs]
}

locals {
  index             = var.index <= 9 ? "0${var.index}" : "${var.index}"
}

resource "google_compute_firewall" "custom-rules" {
  for_each  = var.rules

  name                      = each.value.name
  network                   = var.network
  destination_ranges        = each.value.destination_ranges
  target_tags               = length(var.target_tags) == 0 ? each.value.target_tags : var.target_tags
  source_ranges             = each.value.source_ranges
  source_tags               = each.value.source_tags
  source_service_accounts   = each.value.source_service_accounts
  dynamic "allow" {
    for_each = defaults(each.value.allow, {})
    content {
      protocol    = allow.value.protocol
      ports       = allow.value.ports
    }
  }
  dynamic "deny" {
    for_each = defaults(each.value.deny, {})
    content {
      protocol    = deny.value.protocol
      ports       = deny.value.ports
    }
  }
}
