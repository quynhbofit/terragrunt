variable "index" {
  type          = number
  description   = "Resource index"
  default       = 1
}

variable "network" {
  type          = string
  description   = "The name of the network (vpc) in which firewall rules belong to"
}

variable "target_tags" {
  type          = list(string)
  description   = "A list of instance tags indicating sets of instances"
  default       = []
}

variable "rules" {
  type                      = map(object({
    name                    = string
    direction               = string
    destination_ranges      = optional(list(string))
    target_tags             = optional(list(string))
    source_ranges           = optional(list(string))
    source_service_accounts = optional(list(string))
    source_tags             = optional(list(string))
    allow                   = optional(list(object({
      protocol  = string
      ports     = optional(list(string))
    })))
    deny                    = optional(list(object({
      protocol  = string
      ports     = optional(list(string))
    })))
  }))
}
