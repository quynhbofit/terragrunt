output "ip_address" {
  value = google_compute_address.static-ip-address.address
}

output "self_link" {
  value = google_compute_address.static-ip-address.self_link
}
