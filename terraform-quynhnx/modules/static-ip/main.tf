terraform {
  experiments = [module_variable_optional_attrs]
}

locals {
  index             = var.index <= 9 ? "0${var.index}" : "${var.index}"
}

resource "google_compute_address" "static-ip-address" {
  name          = "${var.static_ip_name}-${local.index}"
  address_type  = var.address_type
  address       = var.address
  purpose       = var.purpose
  subnetwork    = var.subnetwork
  network       = var.network
  prefix_length = var.prefix_length
}
