output "public-ip" {
  value = module.static-public-ip.*.ip_address
}
