##--OS--##
variable "centos7" {
  default = "centos-7-v20200910"
}
variable "centos6" {
  default = "centos-6-v20200910"
}

##--DISK--##
variable "type_disk_ssd" {
  default = "pd-ssd"
}
variable "type_disk_hdd" {
  default = "pd-standard"
}

##--REGION--##
variable "region" {
  default = "asia-southeast1-a"
}
variable "zone_a" {
  default = [ "asia-east1-a", "asia-east2-a", "asia-northeast1-a", "asia-northeast2-a", "asia-northeast3-a", "australia-southeast1-a" ]
}

##--SNAPSHOT--##
variable "snapshot_origin" {
  default = "snapshot-origin"
}
variable "snapshot_mongo" {
  default = "apk02-mongo-01-installed-mongodb"
}
variable "snapshot_vm01" {
  default = "vm01-v1"
}

##--VM01--##
variable "node_count_vm01" {
  default = "2"
}
variable "machine_vm01" {
  default = "g1-small"
}
variable "disk_size_vm01" {
  default = "40"
}

