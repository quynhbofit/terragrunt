# set static IP for instance
resource "google_compute_address" "static-ip-address-vm01" {
  count = var.node_count_vm01
  name  = format("vm01-%02d", count.index + 1)
}
# build instance
resource "google_compute_disk" "vm01" {
  count = var.node_count_vm01
  name  = format("vm01-%02d", count.index + 1)
  type  = var.type_disk_ssd
  # image = var.centos7
  zone  = var.region
  # snapshot =  var.snapshot_vm01
  size = 40
  timeouts {
    create = "60m"
  }
}

# # define disk attach
# resource "google_compute_disk" "vm01_data" {
#   count = var.node_count_vm01
#   name  = format("vm01-data-%02d", count.index + 1)
#   type  = var.type_disk_ssd
#   # image =  var.os_instance #centos-7-v20190619"
#   zone  = var.region[count.index % length(var.region)]
#   # snapshot =  var.snapshot_image
#   size = 100
# }

# define instance
resource "google_compute_instance" "vm01" {
  count                     = var.node_count_vm01
  name                      = format("vm01-%02d", count.index + 1)
  tags                      = ["vm01","http-server"]
  allow_stopping_for_update = true
  machine_type              = var.machine_vm01
  zone                      = var.region
  # metadata = {
  #   serial-port-enable = "true"
  # }
  boot_disk {
    auto_delete = false
    device_name = element(google_compute_disk.vm01.*.name, count.index)
    source      = element(google_compute_disk.vm01.*.self_link, count.index)
  }
  # attached_disk {
  #   source      = element(google_compute_disk.vm01_data.*.self_link, count.index)
  #   device_name = element(google_compute_disk.vm01_data.*.name, count.index)
  # }

  network_interface {
    network = "default"
    access_config {
      nat_ip = element(google_compute_address.static-ip-address-vm01.*.address, count.index)
    }
  }

}
