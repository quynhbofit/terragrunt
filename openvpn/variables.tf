variable "state_bucket" {
  type          = string
  description   = "Bucket name in which store terraform state"
}

variable "state_file_name" {
  type          = string
  description   = "Terraform state file name"
}

variable "encryption_key" {
  type          = string
  description   = "Custom key to encrypt terraform state"
}

variable "instance_name" {
  type          = string
  description   = "GCE instance name"
}

variable "stopping_for_update" {
  type          = bool
  description   = "Allow stop compute engine to update instance"
  default       = false
}

variable "instance_count" {
  type          = number
  description   = "Amount of GCE instances"
}

variable "zone" {
  type          = string
  description   = "Zone in which GCE instance belong to"
  default       = "asia-southeast1-a"
}

variable "machine_type" {
  type          = string
  description   = "GCE instance type"
  default       = "f1-micro"
}

variable "boot_disk_mode" {
  type          = string
  description   = "The mode in which to attach boot disk. READ_WRITE or READ_ONLY"
  default       = "READ_WRITE"
}

variable "boot_disk_size" {
  type          = number
  description   = "The size of the boot disk in gigabytes"
  default       = 30
}

variable "boot_disk_type" {
  type          = string
  description   = "Disk type. May be set to pd-standard, pd-balanced or pd-ssd"
  default       = "pd-balanced"
}

variable "image" {
  type          = string
  description   = "The image from which to initialize boot disk"
  default       = "ubuntu-2004-lts"
}

variable "vpc_name" {
  type          = string
  description   = "The name of the network (vpc) to attach this interface"
}

variable "vpc_subnet_default_name" {
  type          = string
  description   = "The name of the subnetwork (vpc) to attach this interface"
}

variable "network_ip" {
  type          = string
  description   = "The private IP address to assign to the instance"
  default       = "" #If empty, the address will be automatically assigned
}

variable "enable_public_ip" {
  type          = bool
  default       = false
}

variable "alias_ip_range" {
  type          = list(object({
    ip_cidr_range           = string
    subnetwork_range_name   = string
  }))
  description   = "An array of alias IP ranges for this network interface"
  default       = []
}

variable "addition_disks" {
  type          = list(object({
    name        = string
    type        = string
    size        = number
    labels      = optional(map(string))
  }))
  description   = "Add addition persistent disks to your instance"
  default       = []
}

variable "can_ip_forward" {
  type          = bool
  description   = "whether to allow sending and receiving of packets with non-matching source or destination ips"
  default       = false
}

variable "labels" {
  type          = map(string)
  description   = "key/value label pairs to assign to the instance"
  default       = {}
}

variable "ssh_keys" {
  type          = string
  description   = "Add custom ssh key to instance"
  default       = ""
}

variable "tags" {
  type          = list(string)
  description   = "A list of network tags to attach to the instance"
  default       = []
}

variable "static_ip_address_type" {
  type          = string
  description   = "Type of static ip: INTERNAL or EXTERNAL"
  default       = "EXTERNAL"
}

variable "fw_rules" {
  type                      = map(object({
    name                    = string
    direction               = string
    source_ranges           = optional(list(string))
    source_tags             = optional(list(string))
    allow                   = optional(list(object({
      protocol  = string
      ports     = optional(list(string))
    })))
    deny                    = optional(list(object({
      protocol  = string
      ports     = optional(list(string))
    })))
  }))
}

variable "route_openvpn_subnet" {
  type          = string
  description   = "Subnet that OpenVPN server use to connect to client"
}
