data "terraform_remote_state" "network" {
  backend   = "gcs"
  config = {
    bucket          = var.state_bucket
    prefix          = "network/terraform.tfstate"
    encryption_key  = var.encryption_key
  }
}

locals {
  alias_ip_range    = try(var.alias_ip_range, [])
  addition_disks    = {for i, v in var.addition_disks: i => v}
  network_ip        = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+$", var.network_ip)) ? var.network_ip : cidrhost(data.terraform_remote_state.network.outputs.subnet_ranges.default, tonumber(var.network_ip))
  fw_rules          = {for i, v in var.fw_rules: i => merge({for ii, vv in v: ii => vv}, {"name": "${var.vpc_name}-${var.instance_name}-${v.name}"})}
  static_ip_count   = var.enable_public_ip ? var.instance_count : 0
}

module "instance" {
  source                    = "./modules/gce"
  count                     = var.instance_count

  index                     = count.index + 1
  instance_name             = var.instance_name
  allow_stopping_for_update = var.stopping_for_update
  zone                      = var.zone
  machine_type              = var.machine_type
  boot_disk_size            = var.boot_disk_size
  boot_disk_type            = var.boot_disk_type
  image                     = var.image
  vpc_name                  = var.vpc_name
  vpc_subnet_name           = var.vpc_subnet_default_name
  network_ip                = local.network_ip
  alias_ip_range            = var.alias_ip_range
  enable_public_ip          = var.enable_public_ip
  public_ip                 = var.enable_public_ip ? [module.static-public-ip[count.index].ip_address] : null
  addition_disks            = var.addition_disks
  can_ip_forward            = var.can_ip_forward
  labels                    = var.labels
  metadata                  = {ssh-keys = "${var.ssh_keys}"}
  tags                      = var.tags
}

module "firewall" {
  source                    = "./modules/firewall-rule"

  target_tags               = var.tags
  network                   = var.vpc_name
  rules                     = local.fw_rules
}

module "static-public-ip" {
  source                    = "./modules/static-ip"
  count                     = local.static_ip_count

  static_ip_name            = "${var.vpc_name}-${var.instance_name}"
  address_type              = var.static_ip_address_type
}

resource "google_compute_route" "openvpn-subnet" {
  name                      = "to-openvpn-subnet"
  dest_range                = var.route_openvpn_subnet
  network                   = var.vpc_name
  description               = "Route to OpenVPN server subnet"
  next_hop_instance         = module.instance[0].instance_resource_id
}
