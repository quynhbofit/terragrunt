#skip = true
terragrunt_version_constraint = ">= 0.34.0"
terraform_version_constraint = "~> 1.0.8"

locals {
  state_bucket      = "terraform-states-quynhnx"
  state_file_name   = "terraform.tfstate"
  project           = "quynhnx"
  region            = "asia-southeast1"
  encryption_key    = "5lY1q7DRv8hhLziCAzVRhYO1G91HchUVAULrfjTEElU="
  openvpn_subnet    = "192.168.10.0/24"
  k8s_cluster_secondary_range_name  = "gke-cluster-cidr"
  k8s_services_secondary_range_name = "gke-services-cidr"
}

inputs = {
  state_bucket                  = "${local.state_bucket}"
  state_file_name               = "${local.state_file_name}"
  encryption_key                = "${local.encryption_key}"
  project                       = "${local.project}"
  region                        = "${local.region}"
}

remote_state {
  backend = "gcs"
  config = {
    project                     = "${local.project}"
    location                    = "${local.region}"
    skip_bucket_versioning      = true
    enable_bucket_policy_only   = true
    bucket                      = "${local.state_bucket}"
    prefix                      = "${path_relative_to_include()}/${local.state_file_name}"
    encryption_key              = "${local.encryption_key}"
    gcs_bucket_labels = {
      maintainer = "devops"
      created_by = "terragrunt"
    }
  }
  generate = {
    path      = "grunt-backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}

generate "terraform" {
  path      = "grunt-terraform.tf"
  if_exists = "overwrite"

  contents  = <<eof
terraform {
  experiments = [module_variable_optional_attrs]
  required_providers {
    gcp = {
      source = "google"
      version = "= 4.24.0"
    }
    gcp-beta = {
      source = "hashicorp/google-beta"
      version = "= 4.24.0"
    }
  }
}
eof

}

# Configure root level variables that all resources can inherit
terraform {
  extra_arguments "env_common_var" {
    commands            = get_terraform_commands_that_need_vars()
    optional_var_files  = ["${get_parent_terragrunt_dir()}/env-common.tfvars", "${get_parent_terragrunt_dir()}/../common.tfvars"]
  }
}
