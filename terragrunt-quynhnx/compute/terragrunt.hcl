include "env configuration" {
  path = find_in_parent_folders()
}

generate "provider" {
  path      = "grunt-provider.tf"
  if_exists = "overwrite"
  contents  = <<eof
provider "gcp" {
  project       = "quynhnx"
  region        = "asia-southeast1"
  zone          = "asia-southeast1-a"
  credentials   = "${get_env("HOME")}/.config/gcloud/application_default_credentials.json"
}
eof
}

terraform {
  /* source = "${get_parent_terragrunt_dir()}/../../terraform/openvpn" */
  # source = "git::ssh://git@gitlab.g-pay.vn:10022/devops/terraform/gcp.git//openvpn"
  source = "git::ssh://git@gitlab.com:quynhbofit/terragrunt.git//terraform-quynhnx/compute"
}

locals {
  parent_conf   = read_terragrunt_config(find_in_parent_folders(), {inputs = {}})
}

prevent_destroy = true

inputs = {
  instance_name       = "vm01"
  instance_count      = 1
  machine_type        = "custom-1-1024"
  boot_disk_size      = 10
  boot_disk_type      = "pd-balanced"
  network_ip          = "2"
  alias_ip_range      = []
  stopping_for_update = true
  enable_public_ip    = true
  addition_disks      = []
  can_ip_forward      = true
  labels              = {env = "prod"}
  tags                = ["openvpn"]

  fw_rules    = {
    1 = {
      name            = "allow-access-from-office"
      direction       = "INGRESS"
      source_ranges   = ["118.69.125.132/32", "14.0.17.253/32", "118.71.251.140/32", "202.60.110.62/32", "14.0.17.254/32"]
      allow   = [
        {protocol = "tcp", ports = ["22"]},
        {protocol = "icmp"},
        {protocol = "udp", ports = ["1194"]}
      ]
    }
    2 = {
      name            = "allow-connecting-vpn-from-internet"
      direction       = "INGRESS"
      source_ranges   = ["0.0.0.0/0"]
      allow   = [
        {protocol = "udp", ports = ["1194"]}
      ]
    }
  }
  route_openvpn_subnet    = local.parent_conf.locals.openvpn_subnet
}
