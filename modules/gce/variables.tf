variable "index" {
  type          = number
  description   = "Resource index"
  default       = 1
}

variable "instance_name" {
  type          = string
  description   = "GCE instance name"
}

variable "zone" {
  type          = string
  description   = "Zone in which GCE instance belong to"
  default       = "asia-southeast1-a"
}

variable "machine_type" {
  type          = string
  description   = "GCE instance type"
  default       = "f1-micro"
}

variable "allow_stopping_for_update" {
  type          = bool
  description   = "Allow stop compute engine to update instance"
  default       = false
}

variable "boot_disk_mode" {
  type          = string
  description   = "The mode in which to attach boot disk. READ_WRITE or READ_ONLY"
  default       = "READ_WRITE"
}

variable "boot_disk_size" {
  type          = number
  description   = "The size of the boot disk in gigabytes"
  default       = 30
}

variable "boot_disk_type" {
  type          = string
  description   = "Disk type. May be set to pd-standard, pd-balanced or pd-ssd"
  default       = "pd-balanced"
}

variable "image" {
  type          = string
  description   = "The image from which to initialize boot disk"
  default       = "ubuntu-2004-lts"
}

variable "vpc_name" {
  type          = string
  description   = "The name of the network (vpc) to attach this interface"
}

variable "vpc_subnet_name" {
  type          = string
  description   = "The name of the subnetwork (vpc) to attach this interface"
}

variable "network_ip" {
  type          = string
  description   = "The private IP address to assign to the instance"
  default       = "" #If empty, the address will be automatically assigned
}

variable "alias_ip_range" {
  type          = list(object({
    ip_cidr_range           = string
    subnetwork_range_name   = string
  }))
  description   = "An array of alias IP ranges for this network interface"
  default       = []
}

variable "enable_public_ip" {
  type          = bool
  default       = false
}

variable "public_ip" {
  type          = list(string)
  description   = "The IP address that will be 1:1 mapped to the instance's network ip"
  default       = [] #If not given, one will be generated
}

variable "addition_disks" {
  type          = list(object({
    name        = string
    type        = string
    size        = number
    labels      = optional(map(string))
  }))
  description   = "Add addition persistent disks to your instance"
  default       = []
}

variable "can_ip_forward" {
  type          = bool
  description   = "Whether to allow sending and receiving of packets with non-matching source or destination IPs"
  default       = false
}

variable "labels" {
  type          = map(string)
  description   = "key/value label pairs to assign to the instance"
  default       = {}
}

variable "metadata" {
  type          = map(string)
  description   = "key/value pairs to make available from within the instance"
  default       = {}
}

variable "tags" {
  type          = list(string)
  description   = "A list of network tags to attach to the instance"
  default       = []
}
