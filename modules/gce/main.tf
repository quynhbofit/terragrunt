locals {
  alias_ip_range    = try(var.alias_ip_range, [])
  addition_disks    = {for i, v in var.addition_disks: i => v}
  index             = var.index <= 9 ? "0${var.index}" : "${var.index}"
  public_ip         = var.enable_public_ip ? var.public_ip : []
}

terraform {
  experiments = [module_variable_optional_attrs]
}

resource "google_compute_disk" "data" {
  for_each = local.addition_disks

  name      = "${each.value.name}-${local.index}"
  type      = each.value.type
  zone      = var.zone
  size      = each.value.size
  labels    = merge(each.value.labels, {used-by = "${var.instance_name}-${local.index}"})
  physical_block_size_bytes = 4096
}


resource "google_compute_instance" "instance" {
  name                          = "${var.instance_name}-${local.index}"
  zone                          = var.zone
  machine_type                  = var.machine_type
  allow_stopping_for_update     = var.allow_stopping_for_update

  boot_disk {
    auto_delete         = true
    mode                = var.boot_disk_mode #READ_WRITE or READ_ONLY
    initialize_params {
      size      = var.boot_disk_size
      type      = var.boot_disk_type #pd-balanced or pd-standard or pd-ssd
      image     = var.image #ubuntu-2004-focal
    }
  }

  network_interface {
    network         = var.vpc_name
    subnetwork      = var.vpc_subnet_name
    network_ip      = var.network_ip
    dynamic "alias_ip_range" {
      for_each = local.alias_ip_range
      content {
          ip_cidr_range         = alias_ip_range.value.ip_cidr_range
          subnetwork_range_name = alias_ip_range.value.subnetwork_range_name
      }
    }
    dynamic "access_config" {
      for_each = local.public_ip
      content {
        nat_ip  = access_config.value
      }
    }
  }

  dynamic "attached_disk" {
    for_each = google_compute_disk.data
    content {
      source        = attached_disk.value.id
      mode          = "READ_WRITE"
    }
  }
  can_ip_forward    = var.can_ip_forward
  labels            = var.labels
  metadata          = var.metadata
  tags              = concat(var.tags, ["${var.instance_name}-${local.index}"])
}
