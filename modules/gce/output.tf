# output "vpc_id" {
#   value = google_compute_network.vpc-network-default.id
# }
#
# output "vpc_subnet_id" {
#   value = {for name, v in google_compute_subnetwork.vpc-subnet: name => v.id}
# }
#
# output "vpc_subnet_gateway_address" {
#   value = {for name, v in google_compute_subnetwork.vpc-subnet: name => v.gateway_address}
# }
#
# output "addition_disks" {
#   value = {for k, v in google_compute_disk.data: k => v.id }
# }

output "instance_resource_id" {
  value = google_compute_instance.instance.id
}
