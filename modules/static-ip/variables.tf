variable "static_ip_name" {
  type          = string
  description   = "Resource name"
}

variable "index" {
  type          = number
  description   = "Resource index"
  default       = 1
}

variable "address_type" {
  type          = string
  validation {
    condition       = contains(["INTERNAL", "EXTERNAL"], var.address_type)
    error_message   = "Allowed values for input_parameter are: INTERNAL or EXTERNAL."
  }
  description   = "The type of address to reserve"
}

variable "address" {
  type          = string
  description   = "The static external IP address represented by this resource"
  default       = ""
}

variable "purpose" {
  type          = string
  description   = "The purpose of this resource"
  default       = "" #GCE_ENDPOINT, SHARED_LOADBALANCER_VIP, VPC_PEERING, IPSEC_INTERCONNECT or PRIVATE_SERVICE_CONNECT
}

variable "subnetwork" {
  type          = string
  description   = "Subnetwork in which to reserve the address"
  default       = ""
}

variable "network" {
  type          = string
  description   = "The network in which to reserve the address"
  default       = ""
}

variable "prefix_length" {
  type          = number
  description   = "The prefix length if the resource represents an IP range"
  default       = null
}
